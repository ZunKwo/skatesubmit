Rails.application.routes.draw do

  get '/:locale' => 'home#index'

  scope "(:locale)", :locale => /en|fr/ do
    root 'home#index'

    get 'home/index'
    get 'sponsor/index'
    get 'entry/index'

    get 'entry/vote/:id', :to => 'entry#vote#', :as => 'entry_vote'
    post 'entry/send_vote', :as => 'entry_votes'
    get 'entry/send_vote'
    get 'entry/confirm_vote/:id', :to => 'entry#confirm_vote', :as => 'entry_confirm_vote'

    get 'entry/submit', :to => 'entry#submit'
    post 'entry/send_submit', :as => 'skate_entries'
    get 'entry/send_submit'
    get 'entry/confirm_submit/:id', :to => 'entry#confirm_submit', :as => 'entry_confirm_submit'

    get 'entry/pending'
    get 'entry/validate/:id/:value', :to => 'entry#validate'

    get 'sponsor/join', :to => 'sponsor#join'
    post 'sponsor/send_join', :as => 'sponsors'
    get 'sponsor/send_join'
    get 'sponsor/confirm_join/:id', :to => 'sponsor#confirm_join', :as => 'sponsor_confirm_join'

    get 'sponsor/pending'
    get 'sponsor/validate/:id/:value', :to => 'sponsor#validate'

    get 'home/set_lang/:id', :to => 'home#set_lang'

  end



end
