module HomeHelper
  def currentSeasoning
    return EntrySeasoning.find(1)
  end

  def getSeasons
    return EntrySeasoning.where('id > 1').select(:season).distinct.order(season: :desc)
  end

  def getWeekBySeason(season)
    return EntrySeasoning.where('id > 1', season: season).select(:week).distinct.order(week: :desc)
  end
end
