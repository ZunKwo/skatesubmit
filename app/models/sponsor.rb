class Sponsor < ApplicationRecord
  validates :name, :presence => true
  validates :mail, :presence => true, :uniqueness => true
  validates :website, :presence => true
  validates :image, :presence => true

  before_create :confirmation_token

  def sponsor_confirmed
    self.submitted = true
    self.token = SecureRandom.urlsafe_base64.to_s

    self.save!(:validate => false)
  end

  def validate(value)
    self.validated = value
    self.token = nil
    self.save!(:validate => false)
  end

  private
  def confirmation_token
    if self.token.blank?
      self.token = SecureRandom.urlsafe_base64.to_s
    end
  end

  def sponsor_params
    params.require(:sponsor).permit(:name, :mail, :website, :image)
  end
end
