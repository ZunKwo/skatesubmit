class EntryVote < ApplicationRecord
  belongs_to :skate_entry, :foreign_key => :skate_entries_id
  accepts_nested_attributes_for :skate_entry

  validates :mail, :presence => true, :uniqueness => true

  before_create :confirmation_token

  def vote_confirmed
    self.confirmed = true
    self.token = nil
    self.skate_entry.increment_rate

    self.save!(:validate => false)
  end

  private
  def confirmation_token
    if self.token.blank?
      self.token = SecureRandom.urlsafe_base64.to_s
    end
  end
end
