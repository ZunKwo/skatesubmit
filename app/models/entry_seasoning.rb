class EntrySeasoning < ApplicationRecord
  has_many :skate_entries, :dependent => :destroy, :foreign_key => :entry_seasonings_id
end
