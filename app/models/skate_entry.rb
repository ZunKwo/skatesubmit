class SkateEntry < ApplicationRecord
  belongs_to :entry_seasoning, :foreign_key => :entry_seasonings_id
  has_many :entry_votes, :dependent => :destroy, :foreign_key => :skate_entries_id
  accepts_nested_attributes_for :entry_seasoning

  validates :mail, :presence => true, :uniqueness => { :scope => :entry_seasoning }

  before_create :confirmation_token

  def submit_confirmed
    self.submitted = true
    self.token = SecureRandom.urlsafe_base64.to_s

    self.save!(:validate => false)
  end

  def increment_rate
    self.rate += 1

    self.save!(:validate => false)
  end

  def validate(value)
    self.validated = value
    self.token = nil
    self.save!(:validate => false)
  end

  private
  def confirmation_token
    if self.token.blank?
      self.token = SecureRandom.urlsafe_base64.to_s
    end
  end
end
