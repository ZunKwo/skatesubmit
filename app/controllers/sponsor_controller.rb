class SponsorController < ApplicationController
  def index
    @sponsors = Sponsor.where(validated: true).all
  end

  def join
    @sponsor = Sponsor.new
  end

  def send_join
    @sponsor = Sponsor.create(sponsor_params)
    if (@sponsor.save)
      ApplicationMailer.sponsor_confirmation(@sponsor).deliver
    else
      render 'join'
    end
  end

  def confirm_join
    @sponsor = Sponsor.find_by_token(params[:id])
    if @sponsor && (@sponsor.created_at - DateTime.now).hour < 24
      @sponsor.sponsor_confirmed
    end
  end

  def pending
    @sponsors = Sponsor.where(:validated => false)
  end

  def validate
    sponsor = Sponsor.find_by_token(params[:id])
    if sponsor
      sponsor.validate(params[:value])
    end
    if !sponsor.validated
      sponsor.destroy
    end
    redirect_to sponsor_pending_path
  end

  private
  def sponsor_params
    params.require(:sponsor).permit(:name, :mail, :website, :image)
  end
end
