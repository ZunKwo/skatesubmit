class HomeController < ApplicationController
  def index
    currentSeasoning = EntrySeasoning.find(1)
    seasoning = EntrySeasoning.where('id > 1',season: currentSeasoning.season, week: currentSeasoning.week).first
    @entries = SkateEntry.where(entry_seasonings_id: seasoning.id, validated: true).order(rate: :desc, created_at: :desc).limit(4)
    @sponsors = Sponsor.where(validated: true).all.limit(4)
  end
end
