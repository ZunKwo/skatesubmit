class EntryController < ApplicationController
  def index
    currentSeasoning = EntrySeasoning.find(1)
    seasoning = EntrySeasoning.where('id > 1',season: currentSeasoning.season, week: currentSeasoning.week).first
    @entries = SkateEntry.where(entry_seasonings_id: seasoning.id, validated: true).order(rate: :desc, created_at: :desc)
  end

  def vote
    entry = SkateEntry.find(params.required(:id))
    @vote = entry.entry_votes.build
  end

  def send_vote
    entry = SkateEntry.find(params[:entry_vote][:skate_entry_attributes][:id])
    params[:entry_vote].delete(:skate_entry_attributes)
    @vote = entry.entry_votes.create(vote_params)
    if @vote.save
      ApplicationMailer.vote_confirmation(@vote).deliver
    else
      render 'vote'
    end
  end

  def confirm_vote
    @vote = EntryVote.find_by_token(params[:id])
    if @vote && (@vote.created_at - DateTime.now).hour < 24
      @vote.vote_confirmed
    end
  end

  def submit
    seasoning = EntrySeasoning.find(2)
    @entry = seasoning.skate_entries.build
  end

  def send_submit
    seasoning = EntrySeasoning.find(params[:skate_entry][:entry_seasoning_attributes][:id])
    params[:skate_entry].delete(:entry_seasoning_attributes)
    @entry = seasoning.skate_entries.create(skate_entry_params)
    if @entry.save
      ApplicationMailer.entry_confirmation(@entry).deliver
    else
      render 'submit'
    end
  end

  def confirm_submit
    @entry = SkateEntry.find_by_token(params[:id])
    if @entry && (@entry.created_at - DateTime.now).hour < 24
      @entry.submit_confirmed
    end
  end

  def pending
    @entries = SkateEntry.where(:validated => false)
  end

  def validate
    entry = SkateEntry.find_by_token(params[:id])
    if entry
      entry.validate(params[:value])
    end
    if !entry.validated
      entry.destroy
    end
    redirect_to entry_pending_path
  end

  private
  def skate_entry_params
    params.required(:skate_entry).permit(:skater, :link, :mail)
  end

  def vote_params
    params.required(:entry_vote).permit(:mail)
  end
end
