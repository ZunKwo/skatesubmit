class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@lemoosecorp.com'
  layout 'mailer'

  def vote_confirmation(vote)
    @vote = vote
    mail(:to => "Voter <#{vote.mail}>", :subject => "Voting Confirmation", :template_path => 'application_mailer', :template_name => 'vote_confirmation')
  end

  def entry_confirmation(entry)
    @entry = entry
    mail(:to => "Voter <#{entry.mail}>", :subject => "Submitting Confirmation", :template_path => 'application_mailer', :template_name => 'entry_confirmation')
  end

  def sponsor_confirmation(sponsor)
    @sponsor = sponsor
    mail(:to => "Voter <#{sponsor.mail}>", :subject => "Sponsoring Confirmation", :template_path => 'application_mailer', :template_name => 'sponsor_confirmation')
  end
end
