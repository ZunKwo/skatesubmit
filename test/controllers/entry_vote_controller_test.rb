require 'test_helper'

class EntryVoteControllerTest < ActionDispatch::IntegrationTest
  test "should get confirm_vote" do
    get entry_vote_confirm_vote_url
    assert_response :success
  end

end
