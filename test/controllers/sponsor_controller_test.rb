require 'test_helper'

class SponsorControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get sponsor_index_url
    assert_response :success
  end

  test "should get join" do
    get sponsor_join_url
    assert_response :success
  end

  test "should get send_join" do
    get sponsor_send_join_url
    assert_response :success
  end

  test "should get confirm_join" do
    get sponsor_confirm_join_url
    assert_response :success
  end

end
