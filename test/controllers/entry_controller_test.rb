require 'test_helper'

class EntryControllerTest < ActionDispatch::IntegrationTest
  test "should get vote" do
    get entry_vote_url
    assert_response :success
  end

  test "should get send_vote" do
    get entry_send_vote_url
    assert_response :success
  end

  test "should get confirm_vote" do
    get entry_confirm_vote_url
    assert_response :success
  end

  test "should get submit" do
    get entry_submit_url
    assert_response :success
  end

  test "should get send_submit" do
    get entry_send_submit_url
    assert_response :success
  end

  test "should get confirm_submit" do
    get entry_confirm_submit_url
    assert_response :success
  end

end
