require 'test_helper'

class Mailers::VoteMailerControllerTest < ActionDispatch::IntegrationTest
  test "should get vote_confirmation" do
    get mailers_vote_mailer_vote_confirmation_url
    assert_response :success
  end

end
