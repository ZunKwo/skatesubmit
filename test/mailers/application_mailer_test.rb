require 'test_helper'

class ApplicationMailerTest < ActionMailer::TestCase
  test 'sponsor_confirmation' do
    email = ApplicationMailer.create_sponsor_confirmation('me@example.com', 'friend@example.com', Time.now)

    assert_email 1 do
      email.deliver
    end
  end
end
