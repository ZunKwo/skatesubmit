# Preview all emails at http://localhost:3000/rails/mailers/vote_mailer
class ApplicationMailerPreview < ActionMailer::Preview
  def sponsor_confirmation
    ApplicationMailer.sponsor_confirmation(Sponsor.first)
  end
end
