class AddAttrToSkateEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :skate_entries, :mail, :string
    add_column :skate_entries, :token, :string
    add_column :skate_entries, :submitted, :boolean, null: false, default: false
    add_column :skate_entries, :validated, :boolean, null: false, default: false
  end
end
