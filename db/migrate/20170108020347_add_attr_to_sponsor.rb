class AddAttrToSponsor < ActiveRecord::Migration[5.0]
  def change
    add_column :sponsors, :token, :string
    add_column :sponsors, :submitted, :boolean, :null => false, :default => false
    add_column :sponsors, :validated, :boolean, :null => false, :default => false
  end
end
