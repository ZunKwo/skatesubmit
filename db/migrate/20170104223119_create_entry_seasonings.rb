class CreateEntrySeasonings < ActiveRecord::Migration[5.0]
  def change
    create_table :entry_seasonings do |t|
      t.integer :season
      t.integer :week
      t.timestamps
    end
    add_reference :skate_entries, :entry_seasonings, foreign_key: true
  end
end
