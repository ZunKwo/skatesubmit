class CreateSkateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :skate_entries do |t|
      t.string :link
      t.string :skater
      t.integer :rate, :null => false, :default => 0

      t.timestamps
    end
  end
end
