class CreateEntryVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :entry_votes do |t|
      t.string :mail
      t.string :token
      t.boolean :confirmed
      t.timestamps
    end
    add_reference :entry_votes, :skate_entries, foreign_key: true
  end
end
