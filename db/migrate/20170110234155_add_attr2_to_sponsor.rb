class AddAttr2ToSponsor < ActiveRecord::Migration[5.0]
  def change
    add_column :sponsors, :website, :string
    add_column :sponsors, :image, :string
  end
end
