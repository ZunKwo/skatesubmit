# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170111001046) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "application_records", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entry_seasonings", force: :cascade do |t|
    t.integer  "season"
    t.integer  "week"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entry_votes", force: :cascade do |t|
    t.string   "mail"
    t.string   "token"
    t.boolean  "confirmed"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "skate_entries_id"
    t.index ["skate_entries_id"], name: "index_entry_votes_on_skate_entries_id", using: :btree
  end

  create_table "skate_entries", force: :cascade do |t|
    t.string   "link"
    t.string   "skater"
    t.integer  "rate",                default: 0,     null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "entry_seasonings_id"
    t.string   "mail"
    t.string   "token"
    t.boolean  "submitted",           default: false, null: false
    t.boolean  "validated",           default: false, null: false
    t.index ["entry_seasonings_id"], name: "index_skate_entries_on_entry_seasonings_id", using: :btree
  end

  create_table "sponsors", force: :cascade do |t|
    t.string   "name"
    t.string   "mail"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "token"
    t.boolean  "submitted",  default: false, null: false
    t.boolean  "validated",  default: false, null: false
    t.string   "website"
    t.string   "image"
  end

  add_foreign_key "entry_votes", "skate_entries", column: "skate_entries_id"
  add_foreign_key "skate_entries", "entry_seasonings", column: "entry_seasonings_id"
end
